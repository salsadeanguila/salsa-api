FactoryGirl.define do
  factory :user do
    name { Faker::Name.name }
    email 'foo@bar.com'
    password 'foobar'
    user_type 'Admin'
    active true
    gender 'Masculino'
    date_of_birth Date.today
  end
end
