require 'rails_helper'

RSpec.describe Category, type: :model do
  # Association test
  it { should have_many(:items).dependent(:destroy) }

  # Validation test
  it { should validate_presence_of(:name) }

end
