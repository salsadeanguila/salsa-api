require 'rails_helper'

# Test suite for User model
RSpec.describe User, type: :model do
  # Association test
  # Nada aún

  # Validation tests
  # ensure name, email and password_digest are present before save
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
  it { should validate_presence_of(:user_type) }
  it { should validate_presence_of(:active) }
  it { should validate_presence_of(:gender) }
  it { should validate_presence_of(:date_of_birth) }
end
