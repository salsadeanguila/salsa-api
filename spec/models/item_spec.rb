require 'rails_helper'

RSpec.describe Item, type: :model do
  # Association test
  it { should belong_to(:category) }

  # Validation test
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:category_id) }
  it { should validate_presence_of(:active) }
  it { should validate_presence_of(:item_type) }
end
