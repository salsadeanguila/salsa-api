class CategorySerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :description,
             :image_url,
             :presigned_url,
             :menu_order

  has_many :items
end
