class ItemSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :description,
             :category_id,
             :active,
             :price,
             :item_type,
             :image_url,
             :presigned_url

  belongs_to :category
end
