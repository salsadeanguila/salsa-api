class PublicUsersSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :email,
             :active,
             :gender,
             :date_of_birth,
             :created_at
end
