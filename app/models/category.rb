class Category < ApplicationRecord
  include Amazon

  attr_accessor :presigned_url

  has_many :items, dependent: :destroy

  validates_presence_of :name
end
