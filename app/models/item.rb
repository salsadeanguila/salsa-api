class Item < ApplicationRecord
  include Amazon

  attr_accessor :presigned_url

  belongs_to :category

  validates_presence_of :name
  validates_presence_of :category_id
  validates_presence_of :active
  validates_presence_of :item_type
end
