class User < ApplicationRecord
  # encrypt password
  has_secure_password

  # Model associations
  # Aún nada

  # Validations
  validates_presence_of :name,
                        :email,
                        :password_digest,
                        :user_type,
                        :active,
                        :gender,
                        :date_of_birth
  # Fin
end
