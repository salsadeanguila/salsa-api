require 'aws-sdk'
require 'securerandom'
module Amazon
  extend ActiveSupport::Concern

  included do
    after_save :create_presigned_url
    before_destroy :delete_s3_image
  end

  def create_presigned_url
    if self.image_url != nil
      if (self.image_url.include? "salsadeanguila") == false
        region = ENV["REGION"]
        bucket = ENV["S3_BUCKET"]
        filename = "#{self.id}-#{SecureRandom.hex(4)}.jpg"

        s3_client = Aws::S3::Client.new(
          region: region,
          access_key_id: ENV["AWS_ACCESS_KEY_ID"],
          secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"]
        )

        signer = Aws::S3::Presigner.new(client: s3_client)

        presigned_post = signer.presigned_url(
          :put_object,
          bucket: bucket,
          key: filename,
          acl: 'public-read'
        )

        public_url = presigned_post.split('?')[0]

        self.presigned_url = presigned_post
        self.update_column(:image_url, public_url)
      end
    end
  end

  def delete_s3_image
    if self.image_url != nil
      region = ENV["REGION"]
      bucket = ENV["S3_BUCKET"]
      s3_client = Aws::S3::Client.new(
        region: region,
        access_key_id: ENV["AWS_ACCESS_KEY_ID"],
        secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"]
      )
      resp = s3_client.delete_object({
        bucket: bucket,
        key: self.image_url.split('/')[-1]
      })
    end
  end
end
