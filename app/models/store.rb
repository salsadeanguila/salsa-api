class Store < ApplicationRecord


  before_save :primerStore

  validates_presence_of :name, :address

  def primerStore
    if Store.all.length == 0
      self.matrix = true
    end
  end

end
