class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :update, :destroy]
  skip_before_action :authorize_request, only: [:index, :show]

  def index
    @categories = Category.all.order(id: :desc)
    json_response(@categories)
  end

  def create
    @category = Category.create!(category_params)
    json_response(@category, :created)
  end

  def show
    json_response(@category)
  end

  def update
    @category.update(category_params)
    json_response(@category, :ok)
  end

  def destroy
    @category.destroy
    json_response({ :eliminado => true }, :ok)
  end

  private

  def category_params
    params.permit(:id, :name, :description, :image_url, :menu_order)
  end

  def set_category
    @category = Category.find(params[:id])
  end

end
