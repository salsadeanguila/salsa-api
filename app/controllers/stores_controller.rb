class StoresController < ApplicationController
  before_action :set_store, only: [:show, :update, :destroy]
  skip_before_action :authorize_request, only: [:index, :show]

  def index
    @stores = Store.all.order(id: :desc)
    json_response(@stores)
  end

  def create
    @store = Store.create!(store_params)
    json_response(@store, :created)
  end

  def show
    json_response(@store)
  end

  def update
    @store.update(store_params)
    json_response(@store, :ok)
  end

  def destroy
    @store.destroy
    json_response({ :eliminado => true }, :ok)
  end

  private

  def store_params
    params.permit(:id, :name, :description, :matrix, :address,
                  :longitude, :latitude, :email, :phone_number)
  end

  def set_store
    @store = Store.find(params[:id])
  end
end
