class DashboardController < ApplicationController

    # GET /dashboard
  def index
    @totales = {
      :categories => Category.all.length,
      :items => Item.all.length,
      :stores => Store.all.length
    }
    json_response(@totales)
  end

end
