class UsersController < ApplicationController
  skip_before_action :authorize_request, only: :create

  # POST /signup
  # return authenticated token upon signup
  def create
    user = User.create!(user_params)
    auth_token = AuthenticateUser.new(user.email, user.password).call
    response = { message: Message.account_created, auth_token: auth_token }
    json_response(response, :created)
  end

  # GET /users
  # regresa lista de usuarios registrados
  def index
    @users = User.all.order(id: :desc)
    render json: @users, status: :ok, :each_serializer => PublicUsersSerializer
  end

  # POST /user/:id
  def show
    @user = User.find_by_email(user_params[:email])
    json_response(@user, :ok)
  end

  private

  def user_params
    params.permit(
      :name,
      :email,
      :password,
      :password_confirmation,
      :user_type,
      :active,
      :gender,
      :date_of_birth
    )
  end
end
