class ItemsController < ApplicationController
  before_action :set_category, except: [:all]
  before_action :set_category_item, only: [:show, :update, :destroy]
  skip_before_action :authorize_request, only: [:all, :index, :show]

  # GET /items
  def all
    @items = Item.all.order(id: :desc)
    json_response(@items, :ok)
  end

  # GET /categories/:category_id/items
  def index
    json_response(@category.items)
  end

  # GET /categories/:category_id/items/:id
  def show
    json_response(@item)
  end

  # POST /categories/:category_id/items
  def create
    @item = @category.items.create!(item_params)
    json_response(@item, :created)
  end

  # PUT /categories/:category_id/items/:id
  def update
    @item.update(item_params)
    json_response(@item, :ok)
  end

  # DELETE /categories/:category_id/items/:id
  def destroy
    @item.destroy
    json_response({ :eliminado => true }, :ok)
  end

  private

  def item_params
    params.permit(:id, :name, :description, :category_id, :active, :price, :image_url, :item_type)
  end

  def set_category
    @category = Category.find(params[:category_id])
  end

  def set_category_item
    @item = @category.items.find_by!(id: params[:id]) if @category
  end

end
