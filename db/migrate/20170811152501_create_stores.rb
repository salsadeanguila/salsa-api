class CreateStores < ActiveRecord::Migration[5.1]
  def change
    create_table :stores do |t|
      t.string :name, :null => false
      t.text :description
      t.boolean :matrix,  :null => false, :default => false
      t.string :address,  :null => false
      t.float :longitude
      t.float :latitude
      t.string :email
      t.string :phone_number

      t.timestamps
    end
  end
end
