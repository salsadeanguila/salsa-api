class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :name, :null => false
      t.text :description
      t.references :category, foreign_key: true, :null => false
      t.boolean :active, :null => false, :default => true
      t.float :price, :default => '0'
      t.string :image_url
      t.string :item_type, :null => false

      t.timestamps
    end
  end
end
