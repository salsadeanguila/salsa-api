class ChangeCategoryColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :categories, :image, :image_url
  end
end
