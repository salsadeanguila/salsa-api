Rails.application.routes.draw do
  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'
  get 'users', to: 'users#index'
  get 'items', to: 'items#all'
  get 'dashboard', to: 'dashboard#index'
  resources :categories do
    resources :items
  end
  resources :stores
end
